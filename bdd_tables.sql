SET FOREIGN_KEY_CHECKS = 0;
drop table if exists customer;
drop table if exists product;
drop table if exists sale;
drop table if exists tag;
drop table if exists sale_product;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE customer (
    id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    first_name TEXT,
    last_name TEXT,
    mail TEXT,
    phone TEXT,
    city TEXT,
    address TEXT,
    country TEXT
);

CREATE TABLE product (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name varchar(200),
  description TEXT,
  price FLOAT,
  supplier TEXT,
  web_site_supplier TEXT
);

CREATE TABLE sale (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  date_of_purchase DATETIME,
  customer_id INTEGER,
  amount FLOAT,
  FOREIGN KEY (customer_id) REFERENCES customer(id)
);

CREATE TABLE tag (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name TEXT,
  product_id INTEGER,
  FOREIGN KEY (product_id) REFERENCES product(id)
);

CREATE TABLE sale_product (
  id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
  product_id INTEGER,
  sale_id INTEGER,
  quantity INTEGER,
  color TEXT,
  FOREIGN KEY (product_id) REFERENCES product(id),
  FOREIGN KEY (sale_id) REFERENCES sale(id)
);