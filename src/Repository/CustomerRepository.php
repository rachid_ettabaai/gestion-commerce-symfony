<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\CustomerSearch;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    public function findAllwithpaginate()
    {
        return $this->createQueryBuilder("c")->getQuery();
    }

    public function findBywithpaginate(CustomerSearch $customerSearch)
    {
        $query = $this->createQueryBuilder("c");

        if($customerSearch->getFirstname()){

            $query = $query->andwhere("c.firstName = :firstName")
                           ->setParameter("firstName", $customerSearch->getFirstname());
        }

        if($customerSearch->getLastname()) {

            $query = $query->andwhere("c.lastName = :lastName")
                           ->setParameter("lastName", $customerSearch->getLastname());
        }

        return $query->getQuery();
    }

    // /**
    //  * @return Customer[] Returns an array of Customer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Customer
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
