<?php

namespace App\Repository;

use App\Entity\Productoptions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Productoptions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Productoptions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Productoptions[]    findAll()
 * @method Productoptions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoptionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Productoptions::class);
    }

    // /**
    //  * @return Productoptions[] Returns an array of Productoptions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Productoptions
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
