<?php

namespace App\Repository;

use App\Entity\Productoptionsvalue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Productoptionsvalue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Productoptionsvalue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Productoptionsvalue[]    findAll()
 * @method Productoptionsvalue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoptionsvalueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Productoptionsvalue::class);
    }

    // /**
    //  * @return Productoptionsvalue[] Returns an array of Productoptionsvalue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Productoptionsvalue
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
