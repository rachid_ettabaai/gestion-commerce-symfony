<?php

namespace App\Controller\Guest;

use App\Repository\SaleProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SaleproductController extends AbstractController
{
    private $repository;

    public function __construct(SaleProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/saleproducts",name="guest.saleproducts")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $saleproducts = $paginator->paginate(
            $this->repository->findAllwithpaginate(),
            $request->query->getInt('page', 1),
            10
        );
        //dump($saleproducts);
        return $this->render("guest/saleproduct/index.html.twig",compact("saleproducts"));

    }
}