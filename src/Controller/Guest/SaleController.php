<?php 

namespace App\Controller\Guest;

use App\Repository\SaleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SaleController extends AbstractController
{
    private $repository;

    public function __construct(SaleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/sales",name="guest.sales")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $sumsales = $this->repository->calcsum()[0]["sumsale"];
        $sales = $paginator->paginate(
            $this->repository->findAllwithpaginate(),
            $request->query->getInt('page', 1),
            10
        );
        //dump($sumsales);
        return $this->render("guest/sale/index.html.twig",compact("sales","sumsales"));
    }
}
