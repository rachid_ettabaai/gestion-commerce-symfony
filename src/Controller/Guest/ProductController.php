<?php 

namespace App\Controller\Guest;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    /**
     * @Route("/products",name="guest.products")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $products = $this->getDoctrine()->getRepository("App:Product")->findAll();
        //dump($products);
        return $this->render("guest/product/index.html.twig",compact("products"));
    }

}