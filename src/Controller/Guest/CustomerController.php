<?php

namespace App\Controller\Guest;

use App\Entity\CustomerSearch;
use App\Form\CustomerSearchType;
use App\Repository\CustomerRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustomerController extends AbstractController
{

    private $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/customers",name="guest.customers")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {

        $customersearch = new CustomerSearch();
        $form = $this->createForm(CustomerSearchType::class,$customersearch);
        $form->handleRequest($request);
        $forms = $form->createView();

        $customers = $paginator->paginate($this->repository->findBywithpaginate($customersearch),
                                          $request->query->getInt('page', 1),
                                          10);
        //dump($customers);
        return $this->render("guest/customer/index.html.twig",compact("customers","forms"));
    }
}