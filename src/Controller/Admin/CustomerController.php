<?php

namespace App\Controller\Admin;

use App\Entity\Customer;
use App\Form\CustomerType;
use App\Entity\CustomerSearch;
use App\Form\CustomerSearchType;
use App\Repository\CustomerRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustomerController extends AbstractController
{
    private $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/admin/customers",name="admin.customers")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $customersearch = new CustomerSearch();
        $form = $this->createForm(CustomerSearchType::class, $customersearch);
        $form->handleRequest($request);
        $forms = $form->createView();
        
        $customers = $paginator->paginate(
            $this->repository->findBywithpaginate($customersearch),
            $request->query->getInt('page', 1),
            10
        );
        //dump($customers);
        return $this->render("admin/customer/index.html.twig", compact("customers", "forms"));
    }
    /**
     * Form processing for the creation of a new customer
     * @param \Symfony\Component\Form\FormInterface   $form
     * @param Customer $customer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function processform($form,Customer $customer)
    {
        $em = $this->getDoctrine()->getManager();
        if(is_null($customer->getId())){
            $em->persist($customer);
            $this->addFlash("success", "Successful customer creation");
        }else{
            $em->persist($form->getData());
            $this->addFlash("success", "Modification carried out successfully");
        }
        $em->flush();
        
        return $this->redirectToRoute("admin.customers");

    }

    /**
     * @Route("/admin/customers/create",name="admin.customers.create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form,$customer);
        }
        $forms = $form->createView();
        return $this->render("admin/customer/create.html.twig", compact("customer", "forms"));
    }

    /**
     * @Route("/admin/customers/edit/{id}",name="admin.customers.edit")
     * @param Request $request
     * @param Customer $customer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Customer $customer, Request $request)
    {
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form, $customer);
        }
        $forms = $form->createView();
        return $this->render("admin/customer/edit.html.twig", compact("customer", "forms"));
    }

    /**
     * @Route("/admin/customers/delete/{id}",name="admin.customers.delete")
     * @param Request $request
     * @param Customer $customer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Customer $customer, Request $request)
    {
        if ($this->isCsrfTokenValid("delete" . $customer->getId(), $request->get("_token"))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($customer);
            $em->flush();
        }
        return $this->redirectToRoute("admin.customers");
    }

}