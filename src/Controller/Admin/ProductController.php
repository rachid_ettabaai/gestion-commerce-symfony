<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/admin/products",name="admin.products")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $products = $this->repository->findAll();
        return $this->render("admin/product/index.html.twig", compact("products"));
    }

    /**
     * Form processing for the creation of a new customer
     * @param \Symfony\Component\Form\FormInterface   $form
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function processform($form, Product $product)
    {
        $em = $this->getDoctrine()->getManager();
        if (is_null($product->getId())) {
            $em->persist($product);
            $this->addFlash("success", "Successful product creation");
        } else {
            $em->persist($form->getData());
            $this->addFlash("success", "Modification carried out successfully");
        }
        $em->flush();
        return $this->redirectToRoute("admin.products");
    }

    /**
     * @Route("/admin/products/create",name="admin.products.create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form, $product);
        }

        $forms = $form->createView();
        return $this->render("admin/product/create.html.twig", compact("product", "forms"));
    }

    /**
     * @Route("/admin/products/edit/{id}",name="admin.products.edit")
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Product $product, Request $request)
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form, $product);
        }
        $forms = $form->createView();
        return $this->render("admin/product/edit.html.twig", compact("product", "forms"));
    }

    /**
     * @Route("/admin/products/delete/{id}",name="admin.products.delete")
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Product $product, Request $request)
    {
        if ($this->isCsrfTokenValid("delete" . $product->getId(), $request->get("_token"))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }
        return $this->redirectToRoute("admin.products");
    }
}
