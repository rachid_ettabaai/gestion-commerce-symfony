<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productoptions
 *
 * @ORM\Table(name="productoptions")
 * @ORM\Entity(repositoryClass="App\Repository\ProductoptionsRepository")
 */
class Productoptions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=true)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }


}
