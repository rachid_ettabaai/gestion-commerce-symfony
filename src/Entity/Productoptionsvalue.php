<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productoptionsvalue
 *
 * @ORM\Table(name="productoptionsvalue", indexes={@ORM\Index(name="productoption_id", columns={"productoption_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProductoptionsvalueRepository")
 */
class Productoptionsvalue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float|null
     *
     * @ORM\Column(name="value", type="string", length=200, nullable=true)
     */
    private $value;

    /**
     * @var \Productoptions
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Productoptions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productoption_id", referencedColumnName="id")
     * })
     */
    private $productoption;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProductoption(): ?Productoptions
    {
        return $this->productoption;
    }

    public function setProductoption(?Productoptions $productoption): self
    {
        $this->productoption = $productoption;

        return $this;
    }


}
