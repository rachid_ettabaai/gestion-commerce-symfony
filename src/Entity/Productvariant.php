<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productvariant
 *
 * @ORM\Table(name="productvariant", indexes={@ORM\Index(name="product_id", columns={"product_id"}), @ORM\Index(name="productoptionsvalue_id", columns={"productoptionsvalue_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProductvariantRepository")
 */
class Productvariant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=true)
     */
    private $name;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Productoptionsvalue
     *
     * @ORM\ManyToOne(targetEntity="Productoptionsvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productoptionsvalue_id", referencedColumnName="id")
     * })
     */
    private $productoptionsvalue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductoptionsvalue(): ?Productoptionsvalue
    {
        return $this->productoptionsvalue;
    }

    public function setProductoptionsvalue(?Productoptionsvalue $productoptionsvalue): self
    {
        $this->productoptionsvalue = $productoptionsvalue;

        return $this;
    }


}
